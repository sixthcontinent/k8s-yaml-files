server {
    listen 443;
    ssl_certificate /etc/nginx/ssl/six.crt;
    ssl_certificate_key /etc/nginx/ssl/six.key;
    server_name _;

    #location ^~ /api/get_transaction_notifications { return 204; }
    #location ^~ /api/getallnotifications { return 204; }

    location /geoip {
	return 200 "$geoip_country_name\n$geoip_country_code";
    }

    location /health {
        return 204;
    }

    root /var/www/html/web;
    #access_log on;
    #error_log on;
    error_log /var/log/nginx/error.log;
    #access_log /mnt/scdata/logs/nginx/sxbackend.access.log json_combined;
    access_log /var/log/nginx/access.log main;
    #access_log off;

    location /socket.io {
        proxy_read_timeout 300;
        proxy_pass https://127.0.0.1:8000;
    }

    gzip on;
    gzip_comp_level 9;
    gzip_http_version 1.0;
    gzip_proxied any;
    gzip_min_length 100;
    gzip_buffers 16 8k;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript application/octet-stream;
    gzip_disable "MSIE [1-6].(?!.*SV1)";
    client_max_body_size 128m; 
    rewrite ^/app\.php/?(.*)$ /$1 permanent;

    location / {
	    if ($request_method = 'OPTIONS') {
            add_header Access-Control-Allow-Origin '*';
            add_header Access-Control-Allow-Method 'GET, POST, OPTIONS';
            add_header Access-Control-Allow-Headers "Content-Type,X-Country,X-Country-Name";
            add_header Access-Control-Max-Age 1728000;
            add_header Content-Type 'text/plain; charset=utf8';
            add_header Content-Length 0;
            return 204;
        }
        index app.php;
        try_files $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite ^(.*)$ /app_dev.php/$1 last;
    }

    location ~ ^/(app|app_dev|config)\.php(/|$) {
        set $geolock 1;
        
	# EU Country
        if ($geoip_country_code = 'AT') { set $geolock 0; }
        if ($geoip_country_code = 'BE') { set $geolock 0; }
        if ($geoip_country_code = 'BG') { set $geolock 1; }
        if ($geoip_country_code = 'HR') { set $geolock 1; }
        if ($geoip_country_code = 'CY') { set $geolock 1; }
        if ($geoip_country_code = 'CZ') { set $geolock 1; }
        if ($geoip_country_code = 'DK') { set $geolock 0; }
        if ($geoip_country_code = 'EE') { set $geolock 1; }
        if ($geoip_country_code = 'FI') { set $geolock 1; }
        if ($geoip_country_code = 'FR') { set $geolock 0; }
        if ($geoip_country_code = 'DE') { set $geolock 0; }
        if ($geoip_country_code = 'GR') { set $geolock 1; }
        if ($geoip_country_code = 'HU') { set $geolock 1; }
        if ($geoip_country_code = 'IE') { set $geolock 0; }
        if ($geoip_country_code = 'IT') { set $geolock 0; }
        if ($geoip_country_code = 'LV') { set $geolock 1; }
        if ($geoip_country_code = 'LT') { set $geolock 1; }
        if ($geoip_country_code = 'LU') { set $geolock 1; }
        if ($geoip_country_code = 'MT') { set $geolock 1; }
        if ($geoip_country_code = 'NL') { set $geolock 1; }
        if ($geoip_country_code = 'PL') { set $geolock 1; }
        if ($geoip_country_code = 'PT') { set $geolock 0; }
        if ($geoip_country_code = 'RO') { set $geolock 1; }
        if ($geoip_country_code = 'SK') { set $geolock 1; }
        if ($geoip_country_code = 'SI') { set $geolock 0; }
        if ($geoip_country_code = 'ES') { set $geolock 0; }
        if ($geoip_country_code = 'SE') { set $geolock 0; }
        if ($geoip_country_code = 'UK') { set $geolock 1; }
        if ($geoip_country_code = 'GB') { set $geolock 1; }
        # NON EU Country
        if ($geoip_country_code = 'CH') { set $geolock 0; }
        if ($geoip_country_code = 'NO') { set $geolock 0; }
        if ($geoip_country_code = 'RU') { set $geolock 0; }
	if ($geoip_country_code = 'AU') { set $geolock 0; }
	
        #PHILIP
	if ($geoip_country_code = 'MK') { set $geolock 0; } 
	#if ($geolock = 1) { return 403; }
        add_header Access-Control-Allow-Origin "*";
        add_header Access-Control-Allow-Methods "GET,POST,OPTIONS";
        add_header Access-Control-Allow-Headers Content-Type;
        add_header Access-Control-Max-Age 86400;
	    add_header Access-Control-Allow-Headers "Content-Type,X-Country,X-Country-Name";
	    add_header X-Country $geoip_country_code;
        add_header X-Country-Name $geoip_country_name;
	fastcgi_pass 127.0.0.1:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
     	fastcgi_buffers 16 16k;
    	fastcgi_buffer_size 32k;
    	fastcgi_read_timeout 600;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param  HTTPS on;
    }
}